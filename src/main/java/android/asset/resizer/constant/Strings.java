package android.asset.resizer.constant;

public interface Strings {

    String NAME = "Android Asset Resizer";
    String FOLDER = "android-asset-resizer";
    String OUTPUT_DENSITIES = "Output densities";
    String SETTINGS = "Settings";
    String RESOURCES_DIRECTORY = "Output directory";
    String BROWSE = "Browse";
    String NO_DIRECTORY_SELECTED_YET = "Nothing selected yet";
    String INPUT_DENSITY = "Input Density";
    String DESTINATION_FOLDER_NAME = "Destination folder name";
    String DRAG_AND_DROP_MESSAGE = "Drag and drop your image(s) here. :)";
    String SELECT_A_DESTINATION_FOLDER = "Please select a destination folder first!";
    String PROCESSING = "Processing...";
    String FILE_ALREADY_EXISTS = "The file \"%s\" already exists! This image will not be processed.";
    String IO_ERROR_OCCURRED = "An IO error occurred while processing \"%s\".";
    String FILE_IS_NOT_AN_IMAGE = "The file \"%s\" is not a image and will be omitted.";
    String PROCESS_COMPLETED = "Done! Yes yes yes...";
    String ERROR = "Error";
    String WARNING = "Warning";
    String SAME_FOLDER = "The same folder";
    String SELECT_ALL = "Select all";
}