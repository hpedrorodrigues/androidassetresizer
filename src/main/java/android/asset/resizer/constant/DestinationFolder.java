package android.asset.resizer.constant;

public enum DestinationFolder {

    DRAWABLE("drawable"),
    MIPMAP("mipmap"),
    SAME(Strings.SAME_FOLDER);

    private final String title;

    DestinationFolder(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
