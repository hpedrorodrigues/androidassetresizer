package android.asset.resizer.constant;

public enum Density {

    LDPI(3),
    MDPI(4),
    TVDPI(5.33333333f),
    HDPI(6),
    XHDPI(8),
    XXHDPI(12),
    XXXHDPI(16);

    private final float ratio;

    Density(float ratio) {
        this.ratio = ratio;
    }

    public String getResourceName() {
        return name().toLowerCase();
    }

    public float getRatio() {
        return ratio;
    }
}