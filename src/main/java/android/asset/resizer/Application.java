package android.asset.resizer;

import android.asset.resizer.constant.Strings;
import android.asset.resizer.logger.Logger;
import android.asset.resizer.ui.frame.MainFrame;

import javax.swing.*;

public class Application {

    private static final Logger LOGGER = new Logger(Application.class);

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                LOGGER.log("                                                                                  ");
                LOGGER.log("   _           _         _    _     _               _     ___        _            ");
                LOGGER.log("  /_\\  _ _  __| |_ _ ___(_)__| |   /_\\   ______ ___| |_  | _ \\___ __(_)______ _ _ ");
                LOGGER.log(" / _ \\| ' \\/ _` | '_/ _ \\ / _` |  / _ \\ (_-<_-</ -_)  _| |   / -_|_-< |_ / -_) '_|");
                LOGGER.log("/_/ \\_\\_||_\\__,_|_| \\___/_\\__,_| /_/ \\_\\/__/__/\\___|\\__| |_|_\\___/__/_/__\\___|_|  ");
                LOGGER.log("                                                                                  ");
                LOGGER.log("                                                                                  ");
                LOGGER.log(Strings.NAME + " startup! :)");
                LOGGER.log("                                                                                  ");
                LOGGER.log("                                                                                  ");
                LOGGER.log("                                                                                  ");

                MainFrame frame = new MainFrame();
                frame.pack();
                frame.setVisible(true);
            }
        };

        SwingUtilities.invokeLater(runnable);
    }
}