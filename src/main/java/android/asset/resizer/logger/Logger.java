package android.asset.resizer.logger;

import java.util.Date;

public class Logger {

    private final String className;

    public Logger(Class<?> clazz) {
        this.className = clazz.getSimpleName();
    }

    public void log(String message) {
        System.out.println(new Date() + " - " + className + ": " + message);
    }
}