package android.asset.resizer.exception;

public class FileAlreadyExistsException extends Exception {

    private static final long serialVersionUID = 8362230275586119719L;
}