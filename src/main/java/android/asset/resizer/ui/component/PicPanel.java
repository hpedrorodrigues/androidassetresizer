package android.asset.resizer.ui.component;

import android.asset.resizer.logger.Logger;
import android.asset.resizer.util.ImageUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class PicPanel extends JPanel {

    private static final long serialVersionUID = -1413774006756861951L;

    private static final int DEFAULT_SIZE = 300;

    private static final Logger LOGGER = new Logger(PicPanel.class);

    private BufferedImage image;
    private int imageWidth, imageHeight;

    public PicPanel(String fileName) {
        try {
            image = ImageUtil.loadFromResource(fileName);

            if (image == null) {
                imageWidth = DEFAULT_SIZE;
                imageHeight = DEFAULT_SIZE;
            } else {
                imageWidth = image.getWidth();
                imageHeight = image.getHeight();
            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            LOGGER.log("Could not read in the pic");
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(imageWidth, imageHeight);
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        graphics.drawImage(image, 0, 0, this);
    }
}