package android.asset.resizer.ui.presenter;

import android.asset.resizer.constant.Strings;
import android.asset.resizer.exception.FileAlreadyExistsException;
import android.asset.resizer.listener.FileDropListener;
import android.asset.resizer.processor.ImageDetail;
import android.asset.resizer.processor.ImageProcessor;
import android.asset.resizer.ui.frame.MainFrame;
import android.asset.resizer.ui.view.MainView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

public class MainPresenter extends BasePresenter<MainFrame, MainView> {

    private final ImageProcessor imageProcessor;

    public MainPresenter(MainFrame frame) {
        super(frame, new MainView(frame));
        this.imageProcessor = new ImageProcessor();
    }

    @Override
    protected void onConfig() {
        view.setBrowserButtonListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();

                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

                Integer option = fileChooser.showOpenDialog(frame);
                if (option == JFileChooser.APPROVE_OPTION) {
                    view.setDestinationFolder(fileChooser.getSelectedFile());
                    view.setNoDirectorySelectedLabelText(fileChooser.getSelectedFile().getAbsolutePath());
                    frame.pack();
                }
            }
        });

        view.setFileDropListener(new FileDropListener() {

            @Override
            public void onFilesDropped(File[] files) {
                if (!view.hasDestinationFolder()) {
                    view.showWarning(Strings.SELECT_A_DESTINATION_FOLDER);
                    return;
                }

                view.setDragAndDropLabelText(Strings.PROCESSING);
                createAndStartProcessThread(files);
            }
        });

        super.onConfig();
    }

    @Override
    public void startPresenting() {
        super.startPresenting();
    }

    @Override
    public void stopPresenting() {
        super.stopPresenting();
    }

    private void createAndStartProcessThread(final File[] files) {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                Vector<String> export = getExportFolders();

                for (File file : files) {
                    for (String exportString : export) {
                        try {
                            final ImageDetail imageDetail = new ImageDetail();

                            imageDetail.setFile(file);
                            imageDetail.setResourceDirectory(view.getDestinationFolder());
                            imageDetail.setDestinationFolderName(view.getDestinationFolderName());
                            imageDetail.setOriginalSize(view.getOriginalSize());
                            imageDetail.setResFolder(exportString);

                            imageProcessor.processImage(imageDetail);
                        } catch (FileAlreadyExistsException e) {
                            view.showWarning(Strings.FILE_ALREADY_EXISTS.replace("%s", file.getName()));
                            break;
                        } catch (IOException e) {
                            view.showError(Strings.IO_ERROR_OCCURRED.replace("%s", file.getName()));
                            e.printStackTrace();
                            break;
                        } catch (NullPointerException e) {
                            view.showError(Strings.FILE_IS_NOT_AN_IMAGE.replace("%s", file.getName()));
                            e.printStackTrace();
                            break;
                        }

                        view.setDragAndDropLabelText(view.getDragAndDropLabelText() + ".");
                    }
                }

                view.setDragAndDropLabelText(Strings.PROCESS_COMPLETED);
            }
        };

        Thread thread = new Thread(runnable);
        bindProcess(thread);
        thread.start();
    }

    private Vector<String> getExportFolders() {
        Vector<String> ret = new Vector<>();

        java.util.List<JCheckBox> checkboxes = view.getExportCheckboxes();

        for (JCheckBox checkBox : checkboxes) {
            if (checkBox.isSelected()) {
                ret.add(checkBox.getText());
            }
        }

        return ret;
    }
}