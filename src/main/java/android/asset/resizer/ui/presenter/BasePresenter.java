package android.asset.resizer.ui.presenter;

import android.asset.resizer.logger.Logger;
import android.asset.resizer.ui.view.BaseView;

import javax.swing.*;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BasePresenter<F extends JFrame, V extends BaseView<F>> {

    protected final Logger logger;

    protected final F frame;
    protected final V view;

    private final Queue<Thread> processes;

    public BasePresenter(F frame, V view) {
        this.frame = frame;
        this.view = view;
        this.logger = new Logger(this.getClass());
        this.processes = new ConcurrentLinkedQueue<>();
    }

    public void startPresenting() {
        onConfig();
    }

    public void stopPresenting() {
        cancelPendingProcesses();
    }

    protected void onConfig() {
        view.onConfig();
    }

    protected void bindProcess(Thread thread) {
        processes.add(thread);
    }

    private void cancelPendingProcesses() {
        logger.log("Cancelling active thread if exists");

        for (Thread thread : processes) {
            if (thread.isAlive()) {
                thread.interrupt();
            }
        }
    }
}