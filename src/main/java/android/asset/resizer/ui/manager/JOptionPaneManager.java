package android.asset.resizer.ui.manager;

import android.asset.resizer.constant.Strings;

import javax.swing.*;
import java.awt.*;

public class JOptionPaneManager {

    private static void show(final Runnable runnable) {
        // Using SwingUtilities because exists a error in JDK with DND and JOptionPane
        // http://bugs.java.com/view_bug.do?bug_id=4352221
        SwingUtilities.invokeLater(runnable);
    }

    public static void showWarning(final Component parentComponent, final String text) {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                JOptionPane.showMessageDialog(parentComponent, text, Strings.WARNING, JOptionPane.WARNING_MESSAGE);
            }
        };

        show(runnable);
    }

    public static void showError(final Component parentComponent, final String text) {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                JOptionPane.showMessageDialog(parentComponent, text, Strings.ERROR, JOptionPane.ERROR_MESSAGE);
            }
        };

        show(runnable);
    }
}