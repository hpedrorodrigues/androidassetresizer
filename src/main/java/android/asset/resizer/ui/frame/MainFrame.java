package android.asset.resizer.ui.frame;

import android.asset.resizer.constant.Strings;
import android.asset.resizer.ui.presenter.MainPresenter;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFrame extends BaseFrame {

    private static final long serialVersionUID = 5518665390905619611L;

    private final MainPresenter presenter;

    public MainFrame() {
        super(Strings.NAME);

        presenter = new MainPresenter(this);

        presenter.startPresenting();

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                presenter.stopPresenting();
                super.windowClosing(e);
            }
        });
    }
}