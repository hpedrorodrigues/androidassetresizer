package android.asset.resizer.ui.frame;

import javax.swing.*;

public abstract class BaseFrame extends JFrame {

    private static final long serialVersionUID = -48321261897076230L;

    public BaseFrame(String title) {
        super(title);
    }
}