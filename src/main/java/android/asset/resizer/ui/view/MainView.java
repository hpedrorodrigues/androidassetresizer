package android.asset.resizer.ui.view;

import android.asset.resizer.constant.Strings;
import android.asset.resizer.listener.FileDropListener;
import android.asset.resizer.util.ClassUtil;
import android.asset.resizer.util.ImageUtil;
import android.asset.resizer.constant.Density;
import android.asset.resizer.constant.DestinationFolder;
import android.asset.resizer.handler.FileDragAndDropHandler;
import android.asset.resizer.ui.component.PicPanel;
import android.asset.resizer.ui.frame.MainFrame;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

public class MainView extends BaseView<MainFrame> {

    private JPanel contentPanel;
    private JPanel outputDensitiesPanel;
    private JPanel settingsPanel;
    private JButton btnBrowse;
    private JLabel lblNoDirectorySelected;
    private JPanel centralPanel;
    private JLabel lblDragDrop;

    private JComboBox<String> cbInputDensity;
    private JComboBox<String> cbDestinationFolderName;

    private JCheckBox chbXxxhdpi;
    private JCheckBox chbTvdpi;
    private JCheckBox chbLdpi;
    private JCheckBox chbMdpi;
    private JCheckBox chbHdpi;
    private JCheckBox chbXhdpi;
    private JCheckBox chbXxhdpi;
    private JCheckBox selectAllDensities;

    private File destinationFolder;

    private ActionListener browserButtonListener;
    private FileDropListener fileDropListener;

    public MainView(MainFrame frame) {
        super(frame);
    }

    @Override
    public void onConfig() {
        btnBrowse.addActionListener(browserButtonListener);

        new FileDragAndDropHandler(centralPanel, fileDropListener);
    }

    @Override
    protected void onView() {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 700, 700);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        loadIcon();

        createContentPanel();
        createoutputDensitiesPanel();

        createAndAddLDPICheckbox();
        createAndAddMDPICheckbox();
        createAndAddTVDPICheckbox();
        createAndAddHDPICheckbox();
        createAndAddXHDPICheckbox();
        createAndAddXXHDPICheckbox();
        createAndAddXXXHDPICheckbox();

        createAndAddSelectAllCheckboxes();

        createSettingsPanel();
        createLabelResourcesDirectory();
        createButtonBrowseOption();

        createLabelNoDirectorySelectedYet();
        createLabelInputDensity();
        createComboBoxInputDensity();

        createLabelDestinationFolderName();
        createComboBoxDestinationFolderName();

        createLabelAndPanelDragAndDrop();
    }

    private void loadIcon() {
        try {
            BufferedImage image = ImageUtil.loadFromResource("android-icon.png");

            if (ClassUtil.exists("com.apple.eawt.Application")) {
                com.apple.eawt.Application.getApplication().setDockIconImage(image);
            } else {
                frame.setIconImage(image);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createContentPanel() {
        contentPanel = new JPanel();
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

        frame.setContentPane(contentPanel);

        contentPanel.setLayout(new BorderLayout(0, 0));
    }

    private JPanel createoutputDensitiesPanel() {
        outputDensitiesPanel = new JPanel();
        outputDensitiesPanel.setBorder(new TitledBorder(Strings.OUTPUT_DENSITIES));

        GridBagLayout gblExportPanel = new GridBagLayout();

        gblExportPanel.columnWidths = new int[]{0, 0, 0, 0};
        gblExportPanel.rowHeights = new int[]{0, 0, 0};
        gblExportPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gblExportPanel.rowWeights = new double[]{0.0, 0.0, 0.0};

        outputDensitiesPanel.setLayout(gblExportPanel);

        contentPanel.add(outputDensitiesPanel, BorderLayout.SOUTH);

        return outputDensitiesPanel;
    }

    private void createAndAddLDPICheckbox() {
        chbLdpi = new JCheckBox(Density.LDPI.getResourceName());
        chbLdpi.setSelected(true);

        GridBagConstraints gbcLdpi = new GridBagConstraints();

        gbcLdpi.insets = new Insets(0, 0, 5, 5);
        gbcLdpi.anchor = GridBagConstraints.NORTHWEST;
        gbcLdpi.gridx = 0;
        gbcLdpi.gridy = 0;

        outputDensitiesPanel.add(chbLdpi, gbcLdpi);
    }

    private void createAndAddMDPICheckbox() {
        chbMdpi = new JCheckBox(Density.MDPI.getResourceName());
        chbMdpi.setSelected(true);
        chbMdpi.setHorizontalAlignment(SwingConstants.LEFT);

        GridBagConstraints gbcMdpi = new GridBagConstraints();

        gbcMdpi.anchor = GridBagConstraints.NORTHWEST;
        gbcMdpi.insets = new Insets(0, 0, 5, 5);
        gbcMdpi.gridx = 1;
        gbcMdpi.gridy = 0;

        outputDensitiesPanel.add(chbMdpi, gbcMdpi);
    }

    private void createAndAddTVDPICheckbox() {
        chbTvdpi = new JCheckBox(Density.TVDPI.getResourceName());
        chbTvdpi.setSelected(true);

        GridBagConstraints gbcTvdpi = new GridBagConstraints();

        gbcTvdpi.anchor = GridBagConstraints.NORTHWEST;
        gbcTvdpi.insets = new Insets(0, 0, 5, 5);
        gbcTvdpi.gridx = 2;
        gbcTvdpi.gridy = 0;

        outputDensitiesPanel.add(chbTvdpi, gbcTvdpi);
    }

    private void createAndAddHDPICheckbox() {
        chbHdpi = new JCheckBox(Density.HDPI.getResourceName());
        chbHdpi.setSelected(true);

        GridBagConstraints gbcHdpi = new GridBagConstraints();

        gbcHdpi.anchor = GridBagConstraints.NORTHWEST;
        gbcHdpi.insets = new Insets(0, 0, 5, 5);
        gbcHdpi.gridx = 3;
        gbcHdpi.gridy = 0;

        outputDensitiesPanel.add(chbHdpi, gbcHdpi);
    }

    private void createAndAddXHDPICheckbox() {
        chbXhdpi = new JCheckBox(Density.XHDPI.getResourceName());
        chbXhdpi.setSelected(true);

        GridBagConstraints gbcXhdpi = new GridBagConstraints();

        gbcXhdpi.anchor = GridBagConstraints.SOUTHWEST;
        gbcXhdpi.insets = new Insets(0, 0, 5, 5);
        gbcXhdpi.gridx = 0;
        gbcXhdpi.gridy = 1;

        outputDensitiesPanel.add(chbXhdpi, gbcXhdpi);
    }

    private void createAndAddXXHDPICheckbox() {
        chbXxhdpi = new JCheckBox(Density.XXHDPI.getResourceName());
        chbXxhdpi.setSelected(true);

        GridBagConstraints gbcXxhdpi = new GridBagConstraints();

        gbcXxhdpi.anchor = GridBagConstraints.SOUTHWEST;
        gbcXxhdpi.insets = new Insets(0, 0, 5, 5);
        gbcXxhdpi.gridx = 1;
        gbcXxhdpi.gridy = 1;

        outputDensitiesPanel.add(chbXxhdpi, gbcXxhdpi);
    }

    private void createAndAddXXXHDPICheckbox() {
        chbXxxhdpi = new JCheckBox(Density.XXXHDPI.getResourceName());
        chbXxxhdpi.setSelected(true);

        GridBagConstraints gbcXxxhdpi = new GridBagConstraints();

        gbcXxxhdpi.anchor = GridBagConstraints.SOUTHWEST;
        gbcXxxhdpi.insets = new Insets(0, 0, 5, 5);
        gbcXxxhdpi.gridx = 2;
        gbcXxxhdpi.gridy = 1;

        outputDensitiesPanel.add(chbXxxhdpi, gbcXxxhdpi);
    }

    private void createAndAddSelectAllCheckboxes() {
        selectAllDensities = new JCheckBox(Strings.SELECT_ALL);
        selectAllDensities.setSelected(true);

        GridBagConstraints gbcSelectAll = new GridBagConstraints();

        gbcSelectAll.anchor = GridBagConstraints.SOUTHWEST;
        gbcSelectAll.insets = new Insets(0, 0, 5, 5);
        gbcSelectAll.gridx = 3;
        gbcSelectAll.gridy = 1;

        selectAllDensities.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                final boolean isChecked = selectAllDensities.isSelected();

                for (JCheckBox checkBox : getExportCheckboxes()) {
                    checkBox.setSelected(isChecked);
                }
            }
        });

        outputDensitiesPanel.add(selectAllDensities, gbcSelectAll);
    }

    private void createSettingsPanel() {
        settingsPanel = new JPanel();
        settingsPanel.setBorder(new TitledBorder(Strings.SETTINGS));

        GridBagLayout gblSettingsPanel = new GridBagLayout();

        gblSettingsPanel.columnWidths = new int[]{0, 0, 0, 0};
        gblSettingsPanel.rowHeights = new int[]{0, 0, 0};
        gblSettingsPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gblSettingsPanel.rowWeights = new double[]{0.0, 0.0, 0.0};

        settingsPanel.setLayout(gblSettingsPanel);

        contentPanel.add(settingsPanel, BorderLayout.NORTH);
    }

    private void createLabelResourcesDirectory() {
        JLabel lblResourcesDirectory = new JLabel(Strings.RESOURCES_DIRECTORY + ":");

        GridBagConstraints gbcResourcesDirectory = new GridBagConstraints();

        gbcResourcesDirectory.anchor = GridBagConstraints.WEST;
        gbcResourcesDirectory.insets = new Insets(0, 0, 5, 5);
        gbcResourcesDirectory.gridx = 0;
        gbcResourcesDirectory.gridy = 0;

        settingsPanel.add(lblResourcesDirectory, gbcResourcesDirectory);
    }

    private void createButtonBrowseOption() {
        btnBrowse = new JButton(Strings.BROWSE);

        GridBagConstraints gbcBrowse = new GridBagConstraints();

        gbcBrowse.anchor = GridBagConstraints.WEST;
        gbcBrowse.insets = new Insets(0, 0, 5, 5);
        gbcBrowse.gridx = 1;
        gbcBrowse.gridy = 0;

        settingsPanel.add(btnBrowse, gbcBrowse);
    }

    private void createLabelNoDirectorySelectedYet() {
        lblNoDirectorySelected = new JLabel(Strings.NO_DIRECTORY_SELECTED_YET);

        GridBagConstraints gbcNoDirectorySelected = new GridBagConstraints();

        gbcNoDirectorySelected.anchor = GridBagConstraints.WEST;
        gbcNoDirectorySelected.insets = new Insets(0, 0, 5, 0);
        gbcNoDirectorySelected.gridx = 2;
        gbcNoDirectorySelected.gridy = 0;

        settingsPanel.add(lblNoDirectorySelected, gbcNoDirectorySelected);
    }

    private void createLabelInputDensity() {
        JLabel lblInputDensity = new JLabel(Strings.INPUT_DENSITY + ":");

        GridBagConstraints gbcInputDensity = new GridBagConstraints();

        gbcInputDensity.anchor = GridBagConstraints.WEST;
        gbcInputDensity.insets = new Insets(0, 0, 0, 5);
        gbcInputDensity.gridx = 0;
        gbcInputDensity.gridy = 1;

        settingsPanel.add(lblInputDensity, gbcInputDensity);
    }

    private void createComboBoxInputDensity() {
        Vector<String> comboBoxItems = new Vector<>();

        for (Density density : Density.values()) {
            comboBoxItems.add(density.getResourceName());
        }

        final DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(comboBoxItems);

        cbInputDensity = new JComboBox<>(model);
        cbInputDensity.setSelectedIndex(comboBoxItems.size() - 1);

        GridBagConstraints gbcInputDensity = new GridBagConstraints();

        gbcInputDensity.insets = new Insets(0, 0, 0, 5);
        gbcInputDensity.fill = GridBagConstraints.HORIZONTAL;
        gbcInputDensity.gridx = 1;
        gbcInputDensity.gridy = 1;

        settingsPanel.add(cbInputDensity, gbcInputDensity);
    }

    private void createLabelDestinationFolderName() {
        JLabel lblDestinationFolderName = new JLabel(Strings.DESTINATION_FOLDER_NAME + ":");

        GridBagConstraints gbcDestinationFolderName = new GridBagConstraints();

        gbcDestinationFolderName.anchor = GridBagConstraints.WEST;
        gbcDestinationFolderName.insets = new Insets(0, 0, 0, 5);
        gbcDestinationFolderName.gridx = 0;
        gbcDestinationFolderName.gridy = 2;

        settingsPanel.add(lblDestinationFolderName, gbcDestinationFolderName);
    }

    private void createComboBoxDestinationFolderName() {
        Vector<String> comboBoxItems = new Vector<>();

        for (DestinationFolder folder : DestinationFolder.values()) {
            comboBoxItems.add(folder.getTitle());
        }

        final DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(comboBoxItems);

        cbDestinationFolderName = new JComboBox<>(model);
        cbDestinationFolderName.setSelectedIndex(1);

        GridBagConstraints gbcDestinationFolderName = new GridBagConstraints();

        gbcDestinationFolderName.insets = new Insets(0, 0, 0, 5);
        gbcDestinationFolderName.fill = GridBagConstraints.HORIZONTAL;
        gbcDestinationFolderName.gridx = 1;
        gbcDestinationFolderName.gridy = 2;

        settingsPanel.add(cbDestinationFolderName, gbcDestinationFolderName);
    }

    private void createLabelAndPanelDragAndDrop() {
        centralPanel = new PicPanel("android-background.png");
        contentPanel.add(centralPanel, BorderLayout.CENTER);
        centralPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        lblDragDrop = new JLabel(Strings.DRAG_AND_DROP_MESSAGE);
        lblDragDrop.setForeground(Color.DARK_GRAY);
        lblDragDrop.setFont(new Font("Serif", Font.PLAIN, 20));

        centralPanel.add(lblDragDrop, gbc);
    }

    public boolean hasDestinationFolder() {
        return this.destinationFolder != null;
    }

    public void setNoDirectorySelectedLabelText(String text) {
        this.lblNoDirectorySelected.setText(text);
    }

    public void setDragAndDropLabelText(String text) {
        this.lblDragDrop.setText(text);
    }

    public void setBrowserButtonListener(ActionListener browserButtonListener) {
        this.browserButtonListener = browserButtonListener;
    }

    public void setFileDropListener(FileDropListener fileDropListener) {
        this.fileDropListener = fileDropListener;
    }

    public void setDestinationFolder(File destinationFolder) {
        this.destinationFolder = destinationFolder;
    }

    public String getDragAndDropLabelText() {
        return this.lblDragDrop.getText();
    }

    public java.util.List<JCheckBox> getExportCheckboxes() {
        return Arrays.asList(chbLdpi, chbMdpi, chbTvdpi, chbHdpi, chbXhdpi, chbXxhdpi, chbXxxhdpi);
    }

    public File getDestinationFolder() {
        return this.destinationFolder;
    }

    public String getDestinationFolderName() {
        return (String) this.cbDestinationFolderName.getSelectedItem();
    }

    public String getOriginalSize() {
        return (String) this.cbInputDensity.getSelectedItem();
    }
}