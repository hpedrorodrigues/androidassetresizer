package android.asset.resizer.ui.view;

import android.asset.resizer.ui.manager.JOptionPaneManager;

import javax.swing.*;

public abstract class BaseView<F extends JFrame> {

    protected final F frame;

    public BaseView(F frame) {
        this.frame = frame;

        onView();
    }

    public abstract void onConfig();

    protected abstract void onView();

    public void showError(final String message) {
        JOptionPaneManager.showError(frame, message);
    }

    public void showWarning(final String message) {
        JOptionPaneManager.showWarning(frame, message);
    }
}