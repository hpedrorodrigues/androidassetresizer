package android.asset.resizer.listener;

import android.asset.resizer.logger.Logger;
import android.asset.resizer.util.DragEventUtil;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class DropListener implements DropTargetListener {

    private static final Logger LOGGER = new Logger(DropListener.class);

    private static final String ZERO_CHAR_STRING = String.valueOf((char) 0);

    private final Component component;
    private final Border dragBorder;
    private final FileDropListener listener;

    private transient Border normalBorder;

    public DropListener(Component component, Border dragBorder, FileDropListener listener) {
        this.component = component;
        this.dragBorder = dragBorder;
        this.listener = listener;
    }

    @Override
    public void dragEnter(DropTargetDragEvent event) {
        LOGGER.log("DragEnter event.");

        if (DragEventUtil.isDragOk(event)) {

            if (component instanceof JComponent) {
                JComponent jComponent = (JComponent) component;
                normalBorder = jComponent.getBorder();

                LOGGER.log("Normal border saved.");

                jComponent.setBorder(dragBorder);

                LOGGER.log("Drag border set.");
            }

            event.acceptDrag(DnDConstants.ACTION_COPY);

            LOGGER.log("Event accepted.");
        } else {

            event.rejectDrag();
            LOGGER.log("Event rejected.");
        }
    }

    @Override
    public void dragOver(DropTargetDragEvent event) {
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent event) {
        LOGGER.log("DropActionChanged event.");

        if (DragEventUtil.isDragOk(event)) {
            event.acceptDrag(DnDConstants.ACTION_COPY);

            LOGGER.log("Event accepted.");
        } else {
            event.rejectDrag();

            LOGGER.log("Event rejected.");
        }
    }

    @Override
    public void dragExit(DropTargetEvent event) {
        LOGGER.log("DragExit event.");

        if (component instanceof javax.swing.JComponent) {
            JComponent jComponent = (JComponent) component;
            jComponent.setBorder(normalBorder);
            LOGGER.log("Normal border restored.");
        }
    }

    @Override
    public void drop(DropTargetDropEvent event) {
        LOGGER.log("Drop event.");

        try {
            Transferable transferable = event.getTransferable();

            if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                event.acceptDrop(DnDConstants.ACTION_COPY);
                LOGGER.log("File list accepted.");

                List fileList = (List) transferable.getTransferData(DataFlavor.javaFileListFlavor);

                File[] files = new File[fileList.size()];
                fileList.toArray(files);

                if (listener != null) {
                    listener.onFilesDropped(files);
                }

                event.getDropTargetContext().dropComplete(true);
                LOGGER.log("Drop complete.");
            } else {

                DataFlavor[] flavors = transferable.getTransferDataFlavors();
                boolean handled = false;
                for (DataFlavor flavor : flavors) {
                    if (flavor.isRepresentationClassReader()) {
                        event.acceptDrop(DnDConstants.ACTION_COPY);
                        LOGGER.log("Reader accepted.");

                        Reader reader = flavor.getReaderForText(transferable);
                        BufferedReader bufferedReader = new BufferedReader(reader);

                        if (listener != null) {
                            listener.onFilesDropped(createFileArray(bufferedReader));
                        }

                        event.getDropTargetContext().dropComplete(true);
                        LOGGER.log("Drop complete.");
                        handled = true;
                        break;
                    }
                }

                if (!handled) {
                    LOGGER.log("Not a file list or reader - abort.");
                    event.rejectDrop();
                }
            }
        } catch (IOException e) {

            LOGGER.log("IOException - abort:");
            e.printStackTrace();
            event.rejectDrop();
        } catch (UnsupportedFlavorException ex) {

            LOGGER.log("UnsupportedFlavorException - abort:");
            ex.printStackTrace();
            event.rejectDrop();
        } finally {

            if (component instanceof JComponent) {
                JComponent component = (JComponent) this.component;
                component.setBorder(normalBorder);
                LOGGER.log("Normal border restored.");
            }
        }
    }

    private static File[] createFileArray(BufferedReader bReader) {
        try {
            List<File> list = new ArrayList<>();
            String line;

            while ((line = bReader.readLine()) != null) {
                try {
                    if (ZERO_CHAR_STRING.equals(line)) {
                        continue;
                    }

                    list.add(new File(new URI(line)));
                } catch (Exception e) {
                    LOGGER.log("Error with " + line + ": " + e.getMessage());
                }
            }
            return list.toArray(new File[list.size()]);
        } catch (IOException ex) {
            LOGGER.log("IOException");
            ex.printStackTrace();
        }

        return new File[0];
    }
}