package android.asset.resizer.listener;

import java.io.File;

public interface FileDropListener {

    void onFilesDropped(File[] files);
}