package android.asset.resizer.util;

import android.asset.resizer.logger.Logger;

import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTargetDragEvent;

public class DragEventUtil {

    private static final Logger LOGGER = new Logger(DragEventUtil.class);

    /**
     * Determine if the dragged data is a file list.
     */
    public static boolean isDragOk(final DropTargetDragEvent evt) {
        boolean draggedDataIsAFileList = false;

        DataFlavor[] flavors = evt.getCurrentDataFlavors();

        for (DataFlavor flavor : flavors) {

            if (flavor.equals(DataFlavor.javaFileListFlavor) || flavor.isRepresentationClassReader()) {
                draggedDataIsAFileList = true;
                break;
            }
        }

        if (flavors.length == 0) {
            LOGGER.log("No data flavors.");
        } else {

            for (DataFlavor flavor : flavors) {
                LOGGER.log("Flavor: " + flavor.toString());
            }
        }

        return draggedDataIsAFileList;
    }
}