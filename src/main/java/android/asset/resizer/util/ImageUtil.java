package android.asset.resizer.util;

import android.asset.resizer.logger.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ImageUtil {

    private static final Logger LOGGER = new Logger(ImageUtil.class);

    public static BufferedImage loadFromResource(String fileName) throws IOException {
        URL url = ImageUtil.class.getClassLoader().getResource(fileName);

        if (url == null) {
            LOGGER.log("Invalid file name: " + fileName);
            return null;
        }

        return ImageIO.read(url);
    }

    public static BufferedImage loadFromFile(File file) throws IOException {
        return ImageIO.read(file);
    }
}