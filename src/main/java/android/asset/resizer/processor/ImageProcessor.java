package android.asset.resizer.processor;

import android.asset.resizer.constant.DestinationFolder;
import android.asset.resizer.constant.Strings;
import android.asset.resizer.util.ImageUtil;
import android.asset.resizer.constant.Density;
import android.asset.resizer.exception.FileAlreadyExistsException;
import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.ResampleFilters;
import com.mortennobel.imagescaling.ResampleOp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageProcessor {

    public void processImage(ImageDetail detail)
            throws FileAlreadyExistsException, IOException, NullPointerException {

        String finalPath = getFinalPath(detail);

        File destFile = new File(finalPath);
        if (destFile.exists()) {
            throw new FileAlreadyExistsException();
        }

        destFile.getParentFile().mkdirs();

        BufferedImage image = ImageUtil.loadFromFile(detail.getFile());

        int destWidth = getRequiredSize(detail.getOriginalSize(), detail.getResFolder(), image.getWidth());
        int destHeight = destWidth * image.getHeight() / image.getWidth();

        ResampleOp resampleOp = new ResampleOp(destWidth, destHeight);
        resampleOp.setFilter(ResampleFilters.getLanczos3Filter());
        resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.None);

        image = resampleOp.filter(image, null);

        String extension = getExtension(detail.getFile().getName());

        ImageIO.write(image, extension, destFile);
    }

    private int getRequiredSize(String originalSize, String resFolder, int width) {
        float destRatio = 1, origRatio = 1;

        for (Density density : Density.values()) {

            if (resFolder.equalsIgnoreCase(density.getResourceName())) {
                destRatio = density.getRatio();
            }

            if (originalSize.equalsIgnoreCase(density.getResourceName())) {
                origRatio = density.getRatio();
            }
        }

        return Math.round(((float) width) * destRatio / origRatio);
    }

    private String getExtension(String fileName) {
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }

        return extension;
    }

    private String getFinalPath(ImageDetail detail) {
        String finalPath = detail.getResourceDirectory().getAbsolutePath() + File.separator;

        if (detail.getDestinationFolderName().equals(DestinationFolder.SAME.getTitle())) {

            finalPath += Strings.FOLDER + File.separator +
                    detail.getResFolder() + "-" + detail.getFile().getName();
        } else {
            finalPath += detail.getDestinationFolderName() + "-" +
                    detail.getResFolder() + File.separator +
                    detail.getFile().getName();
        }

        return finalPath;
    }
}