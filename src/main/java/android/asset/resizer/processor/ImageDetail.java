package android.asset.resizer.processor;

import java.io.File;

public class ImageDetail {

    private File file;
    private File resourceDirectory;
    private String destinationFolderName;
    private String originalSize;
    private String resFolder;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getResourceDirectory() {
        return resourceDirectory;
    }

    public void setResourceDirectory(File resourceDirectory) {
        this.resourceDirectory = resourceDirectory;
    }

    public String getDestinationFolderName() {
        return destinationFolderName;
    }

    public void setDestinationFolderName(String destinationFolderName) {
        this.destinationFolderName = destinationFolderName;
    }

    public String getOriginalSize() {
        return originalSize;
    }

    public void setOriginalSize(String originalSize) {
        this.originalSize = originalSize;
    }

    public String getResFolder() {
        return resFolder;
    }

    public void setResFolder(String resFolder) {
        this.resFolder = resFolder;
    }
}