package android.asset.resizer.handler;

import android.asset.resizer.listener.DropListener;
import android.asset.resizer.listener.FileDropListener;
import android.asset.resizer.logger.Logger;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.util.TooManyListenersException;

public class FileDragAndDropHandler {

    private static final Logger LOGGER = new Logger(FileDragAndDropHandler.class);

    private static final Color DEFAULT_BORDER_COLOR = new Color(0f, 0f, 1f, 0.25f);

    private transient DropTargetListener dropListener;

    public FileDragAndDropHandler(final Component component, final FileDropListener listener) {
        this(component, BorderFactory.createMatteBorder(2, 2, 2, 2, DEFAULT_BORDER_COLOR), false, listener);
    }

    public FileDragAndDropHandler(final Component component, final Border dragBorder,
                                  final boolean recursive, final FileDropListener listener) {

        if (listener == null) {
            throw new NullPointerException("FileDropListener cannot be null");
        }

        dropListener = new DropListener(component, dragBorder, listener);

        makeDropTargetRecursively(component, recursive);
    }

    private void makeDropTargetRecursively(final Component component, boolean recursive) {
        final DropTarget dropTarget = new DropTarget();

        try {
            dropTarget.addDropTargetListener(dropListener);
        } catch (TooManyListenersException e) {
            e.printStackTrace();
            LOGGER.log("Drop will not work due to previous error. Do you have another listener attached?");
        }

        // Listen for hierarchy changes and remove the drop target when the parent gets cleared out.
        component.addHierarchyListener(new HierarchyListener() {

            @Override
            public void hierarchyChanged(HierarchyEvent event) {
                LOGGER.log("Hierarchy changed.");

                Component parent = component.getParent();

                if (parent == null) {

                    component.setDropTarget(null);
                    LOGGER.log("Drop target cleared from component.");
                } else {

                    new DropTarget(component, dropListener);
                    LOGGER.log("Drop target added to component.");
                }
            }
        });

        if (component.getParent() != null) {
            new DropTarget(component, dropListener);
        }

        if (recursive && (component instanceof Container)) {
            Container container = (Container) component;
            Component[] components = container.getComponents();

            for (Component comp : components) {
                makeDropTargetRecursively(comp, true);
            }
        }
    }

    /**
     * Removes the drag-and-drop hooks from the component and optionally
     * from the all children. You should call this if you add and remove
     * components after you've set up the drag-and-drop.
     * This will recursively unregister all components contained within
     * <var>c</var> if <var>c</var> is a {@link java.awt.Container}.
     */
    public static boolean remove(Component component) {
        return remove(component, true);
    }

    /**
     * Removes the drag-and-drop hooks from the component and optionally
     * from the all children. You should call this if you add and remove
     * components after you've set up the drag-and-drop.
     */
    public static boolean remove(Component component, boolean recursive) {
        LOGGER.log("Removing drag-and-drop hooks.");
        component.setDropTarget(null);

        if (recursive && (component instanceof Container)) {
            Component[] components = ((Container) component).getComponents();

            for (Component comp : components) {
                remove(comp);
            }

            return true;
        } else {

            return false;
        }
    }
}