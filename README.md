# Android Asset Resizer

A simple application to help you resize your android image resources. :)

Based in [Final-Android-Resizer](https://github.com/asystat/Final-Android-Resizer).

Download the **JAR** file [here](https://github.com/hpedrorodrigues/AndroidAssetResizer/releases/download/v1.0/android-asset-resizer-1.0.jar).

## License

Android Asset Resizer is released under the MIT license. See 
[LICENSE](./LICENSE) for details.

## More

Android Asset Resizer is a work in progress, feel free to improve it.